﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveTarget : MonoBehaviour {

	private bool _isDying = false;
	private bool _isDead = false;

	private float _fallingSpeed = 1.0f;

	void Update() {
		if (_isDying) {
			WanderingAI behavior = GetComponent<WanderingAI> ();
			this.transform.Rotate (_fallingSpeed, 0, 0);
		}
	}

	public void ReactToHit () {
		WanderingAI behavior = GetComponent<WanderingAI> ();
		if (behavior != null) {
			behavior.SetAlive (false);
		}
		//_isDying = true;
		StartCoroutine (Die ());
	}

	private IEnumerator Die () {
		this.transform.Rotate (-75, 0, 0);
		yield return new WaitForSeconds (1.5f);
		Destroy (this.gameObject);
	}		
}
